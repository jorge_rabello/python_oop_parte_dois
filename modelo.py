class Programa:
    def __init__(self, nome, ano):
        self._nome = nome.title()
        self.ano = ano
        self._likes = 0

    @property
    def likes(self):
        return self._likes

    @property
    def nome(self):
        return self._nome

    def dar_like(self):
        self._likes += 1

    @nome.setter
    def nome(self, nome):
        self._nome = nome.title()

    def __str__(self) -> str:
        return f'Nome: {self._nome} - Ano: {self.ano} - Duração: {self.duracao} - Likes: {self._likes}'


class Filme (Programa):
    def __init__(self, nome, ano, duracao):
        super().__init__(nome, ano)
        self.duracao = duracao

    def __str__(self) -> str:
        return f'Nome: {self._nome} - Ano: {self.ano} - Duração: {self.duracao} - Likes: {self._likes}'


class Serie(Programa):
    def __init__(self, nome, ano, temporadas):
        super().__init__(nome, ano)
        self.temporadas = temporadas

    def __str__(self) -> str:
        return f'Nome: {self._nome} - Ano: {self.ano} - Duração: {self.temporadas} - Likes: {self._likes}'


class Playlist:
    def __init__(self, nome, programas):
        self.nome = nome
        self._programas = programas

    def __getitem__(self, item):
        return self._programas[item]

    @property
    def listagem(self):
        return self._programas

    def __len__(self):
        return len(self._programas)


vingadores = Filme('Vingadores Guerra Infinita', '2018', 160)
bagulhos_estranhos = Serie('Stranger Things', '2017', 3)
todo_mundo_em_panico = Filme('Todo Mundo em Pânico', 1999, 100)
demolidor = Serie('Demolidor', 2016, 2)

vingadores.dar_like()
vingadores.dar_like()
bagulhos_estranhos.dar_like()

todo_mundo_em_panico.dar_like()
todo_mundo_em_panico.dar_like()
todo_mundo_em_panico.dar_like()
todo_mundo_em_panico.dar_like()

demolidor.dar_like()
demolidor.dar_like()
demolidor.dar_like()
demolidor.dar_like()
demolidor.dar_like()
demolidor.dar_like()
demolidor.dar_like()
demolidor.dar_like()

bagulhos_estranhos.nome = "bagulhos parte 2"

filmes_e_series = [vingadores, bagulhos_estranhos,
                   demolidor, todo_mundo_em_panico]

playlist_fim_de_semana = Playlist('Fim de Semana', filmes_e_series)

print(f'Tamanho da playlist: {len(playlist_fim_de_semana)}')
print(vingadores in playlist_fim_de_semana)
print(demolidor in playlist_fim_de_semana)
print(playlist_fim_de_semana[0])

for programa in playlist_fim_de_semana:
    print(programa)

print(f'Tá ou não tá ? {demolidor in playlist_fim_de_semana}')
